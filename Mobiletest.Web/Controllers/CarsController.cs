﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Mobiletest.Web.ViewModels.Cars;
using System.Data.SqlClient;
using System.Configuration;

namespace Mobiletest.Web.Controllers
{
    public class CarsController : Controller
    {
        //
        // GET: /Cars/

        public ActionResult Index()
        {
            var model = new IndexViewModel();
            var cars = new List<Car>();
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appharbordata"].ConnectionString))
            {
                conn.Open();
                using (var command = new SqlCommand("SELECT * FROM Cars", conn))
                {
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            cars.Add(new Car { CarId = (int)reader["carId"], Name = reader["name"].ToString() });
                        }
                    }
                }
            }
            model.Cars = cars;
            return View(model);
        }

    }
}
