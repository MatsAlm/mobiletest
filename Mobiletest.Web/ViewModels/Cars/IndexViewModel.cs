﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mobiletest.Web.ViewModels.Cars
{
    public class Car
    {
        public int CarId { get; set; }
        public string Name { get; set; }
    }
    public class IndexViewModel
    {
        public IEnumerable<Car> Cars { get; set; }
    }
}