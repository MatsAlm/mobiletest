﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SiteMaster.Master" Inherits="System.Web.Mvc.ViewPage<Mobiletest.Web.ViewModels.Home.IndexViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NavBar" runat="server">
<ul>
		<li><a href="/Home" id="home-button" data-icon="grid" class="ui-btn-active">Hem</a></li>
		<li><a href="/Cars" id="cars-button" data-icon="search">Bilar</a></li>
        <li><a href="/Maps" id="maps-button" data-icon="info">Karta</a></li>
	</ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div data-role="content" id="home-page">
<h2>Välkommen...</h2>
<p>Detta är en ASP.NET MVC 2 sajt vars källkod ligger på <a href="http://www.bitbucket.org/MatsAlm/mobiletest">bitbucket.org/MatsAlm/mobiletest</a></p>
<p>Fysiskt körs sajten på <a href="http://www.appharbor.com">appharbor</a>. Bitbucket är konfigurerat via en service hook att notifiera appharbor vid git push, var på bygge/deploy körs automatiskt.</p>
</div>
</asp:Content>
