﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SiteMaster.Master" Inherits="System.Web.Mvc.ViewPage<Mobiletest.Web.ViewModels.Cars.IndexViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Mobile test
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NavBar" runat="server">
<ul>
		<li><a href="/Home" id="home-button" data-icon="grid">Hem</a></li>
		<li><a href="/Cars" id="cars-button" data-icon="search" class="ui-btn-active">Bilar</a></li>
        <li><a href="/Maps" id="maps-button" data-icon="info">Karta</a></li>
	</ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div data-role="content" id="cars-page">
		<p>Bilar till salu</p>
        <ul data-role="listview" data-inset="true" data-filter="true">
	<%foreach (var car in Model.Cars)
   { %>
    <li><a href="#"><%=car.Name %></a></li>
	<%} %>
</ul>
		<form>
           <label for="slider-0">Input slider:</label>
           <input type="range" name="slider" id="slider-0" value="25" min="0" max="100"  />
        </form>
</div>
</asp:Content>
