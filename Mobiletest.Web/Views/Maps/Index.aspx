﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SiteMaster.Master" Inherits="System.Web.Mvc.ViewPage<Mobiletest.Web.ViewModels.Maps.IndexViewModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Karta
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="NavBar" runat="server">
<ul>
		<li><a href="/Home" id="home-button" data-icon="grid">Hem</a></li>
		<li><a href="/Cars" id="cars-button" data-icon="search">Bilar</a></li>
        <li><a href="/Maps" id="maps-button" data-icon="info" class="ui-btn-active">Karta</a></li>
	</ul>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div data-role="content" id="map_canvas" style="width:100%;height:300px"></div>
    <script type="text/javascript">
        pageScript(function (context) {
            if (!navigator.geolocation) {
                alert("geolocation is not available");
            }
            else {
                navigator.geolocation.getCurrentPosition(function (position) {
                    var lat = position.coords.latitude;
                    var lng = position.coords.longitude;
                    var alt = position.coords.altitude;
                    initializeMap(lat, lng);
                    //$("#position").text("success, lat: " + lat + ", lng: " + lng + ", alt: " + alt);
                }, function (error) { alert("error: code" + error.code + ", message" + error.message); });
            }
        });

       function initializeMap(lat, lng) {
           var myOptions = {
               center: new google.maps.LatLng(lat, lng),
               zoom: 8, mapTypeId: google.maps.MapTypeId.ROADMAP
           };
           var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
           var marker = new google.maps.Marker(
           {
            position: new google.maps.LatLng(lat, lng),
            map: map
           });
           google.maps.event.trigger(map, 'resize');
           }
    </script>
</asp:Content>
